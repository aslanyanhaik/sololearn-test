//
//  UIViewControllerExtension.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/21/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

extension UIViewController {
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
