//
//  UILabelExtension.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/27/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

extension UILabel {
    
    func set(text: String?, highlightText: String, color: UIColor) {
        let predicate = NSPredicate(format:"SELF == %@", highlightText.lowercased())
        let attributedString = NSMutableAttributedString()
        let defaultAttributes = [NSAttributedString.Key.backgroundColor: UIColor.white, NSAttributedString.Key.foregroundColor: UIColor.black]
        let highLightedAttributes = [NSAttributedString.Key.backgroundColor: color, NSAttributedString.Key.foregroundColor: UIColor.white]
        text?.components(separatedBy: .whitespaces).forEach({ (word) in
            if predicate.evaluate(with: word.lowercased()) {
                attributedString.append(NSAttributedString(string: word, attributes: highLightedAttributes))
                attributedString.append(NSAttributedString(string: " ", attributes: defaultAttributes))
            } else {
                attributedString.append(NSAttributedString(string: word, attributes: defaultAttributes))
                attributedString.append(NSAttributedString(string: " ", attributes: defaultAttributes))
            }
        })
        attributedText = attributedString
    }
}
