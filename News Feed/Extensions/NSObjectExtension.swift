//
//  NSObjectExtension.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/21/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

extension NSObject {
    class var className: String {
        return String(describing: self.self)
    }
}
