//
//  URLBuilder.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

struct URLBuilder {
    
    static let APIEntryPoint = "https://content.guardianapis.com"
    static let APIKey = "4aeb4a02-5c73-4e3d-ae09-ad6c084734e7"
    
    static func url(location: APIEndPoint, parameters: [String: String]? = nil) -> String {
        var url = APIEntryPoint + location.rawValue
        var urlParameters = [String: String]()
        urlParameters["api-key"] = APIKey
        parameters?.forEach({urlParameters[$0.key] = $0.value})
        for (index, parameter) in urlParameters.enumerated() {
            guard let parameterKey = parameter.key.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed), let parameterValue = parameter.value.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) else {
                continue
            }
            url += index == 0 ? "?" : "&"
            url += parameterKey + "=" + parameterValue
        }
        return url
    }
}

enum APIEndPoint: String {
    case content = "/search"
    case tags = "/tags"
    case sections = "/sections"
    case editions = "/editions"
}
