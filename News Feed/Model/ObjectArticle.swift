//
//  ObjectArticle.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

class ObjectArticle: Decodable, Hashable, Equatable {
    
    var id: String?
    var title: String?
    var body: String?
    var thumbnailURLString: String?
    var date: Date?
    var categoryName: String?
    lazy var tags: [String] = {
        var tags = title?.components(separatedBy: .whitespacesAndNewlines).shuffled().filter({$0.count > 1}) ?? [String]()
        tags = tags.map({return "#" + $0})
        return tags
    }()
    lazy var topWords: [ObjectWord] = {
        var words = Set<ObjectWord>()
        body?.components(separatedBy: .whitespacesAndNewlines).forEach({ (text) in
            let word = ObjectWord(word: text, count: 0)
            if words.contains(word), var containingWord = words.filter({$0.word == word.word}).first {
                containingWord.count += 1
                words.update(with: containingWord)
            } else {
                words.insert(word)
            }
        })
        return Array(words.sorted(by: {$0.count > $1.count}).filter({$0.count >= 10}))
    }()
    var hashValue: Int {
        return id.hashValue
    }
    
    static func == (lhs: ObjectArticle, rhs: ObjectArticle) -> Bool {
        return lhs.id == rhs.id
    }
    
    public required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self)
        title = try container.decodeIfPresent(String.self, forKey: .title)
        categoryName = try container.decodeIfPresent(String.self, forKey: .category)
        id = try container.decodeIfPresent(String.self, forKey: .id)
        let fieldsContainer = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .fields)
        body = try fieldsContainer.decodeIfPresent(String.self, forKey: .bodyText)
        thumbnailURLString = try fieldsContainer.decodeIfPresent(String.self, forKey: .thumbnail)
        if let dateString = try container.decodeIfPresent(String.self, forKey: .date) {
            let formatter = ISO8601DateFormatter()
            date = formatter.date(from: dateString)
        }
    }
}
