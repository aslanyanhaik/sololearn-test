//
//  ObjectWord.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/27/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

struct ObjectWord: Hashable, Equatable {
    
    var word: String
    var count: Int
    var hashValue: Int {
        return word.hashValue
    }
    
    static func == (lhs: ObjectWord, rhs: ObjectWord) -> Bool {
        return lhs.word == rhs.word
    }
}
