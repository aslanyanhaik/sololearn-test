//
//  GenericObjectsContainer.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

class GenericObjectsContainer<Element: Decodable>: Decodable {
    
    var results = [Element]()
    
    public required convenience init(from decoder: Decoder) throws {
        self.init()
        let container = try decoder.container(keyedBy: CodingKeys.self).nestedContainer(keyedBy: CodingKeys.self, forKey: .response)
        results = try container.decode(Array<Element>.self, forKey: .results)
    }
}

