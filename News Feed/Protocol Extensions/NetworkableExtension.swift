//
//  NetworkableExtension.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

extension Networkable {
    
    func objects<T>(_ objectType: T.Type, urlString: String, completion: @escaping CompletionBlock<T>) where T: Decodable {
        guard let url = URL(string: urlString) else { completion(.failure(nil)); return }
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            DispatchQueue.main.async {
                guard error == nil else { completion(.failure(error)); return }
                guard let data = data else { completion(.failure(error)); return }
                if let result = try? JSONDecoder().decode(GenericObjectsContainer<T>.self, from: data) {
                    completion(.objects(result.results))
                }
            }
        }.resume()
    }
}


