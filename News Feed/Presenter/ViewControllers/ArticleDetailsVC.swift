//
//  ArticleDetailsVC.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/23/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleDetailsVC: UIViewController {
    
    var article: ObjectArticle?
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tableView: ResizeableUITableView!
    @IBOutlet weak var bodyLabel: UILabel!
    
    func customization() {
        if let flow = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            flow.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        }
        collectionView.reloadData()
        tableView.reloadData()
    }
    
    func setArticleData() {
        titleLabel.text = article?.title
        bodyLabel.text = article?.body
        if let urlString = article?.thumbnailURLString {
            thumbnailImageView.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "articlePlaceholder"))
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        collectionView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customization()
        setArticleData()
    }
}

extension ArticleDetailsVC: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return article?.tags.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TagCell.className, for: indexPath) as! TagCell
        cell.set(article?.tags[indexPath.row])
        return cell
    }
}

extension ArticleDetailsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return article?.topWords.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: WordCell.className) as! WordCell
        cell.set(article?.topWords[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let word = article?.topWords[indexPath.row].word else { return }
        bodyLabel.set(text: article?.body, highlightText: word, color: UIColor(red: 240.0/255.0, green: 180.0/255.0, blue: 2.0/255.0, alpha: 1))
    }
}
