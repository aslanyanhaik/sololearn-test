//
//  CategoryPickerVC.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/21/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class CategoryPickerVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var categories = [ObjectCategory]()
    var closure: ((ObjectCategory) -> Void)?
    
    @IBAction func hidePicker(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    class func show<T: UIViewController>(from: T, categories: [ObjectCategory], completion: ((ObjectCategory) -> Void)?) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: CategoryPickerVC.className) as! CategoryPickerVC
        vc.modalPresentationStyle = .overFullScreen
        vc.modalTransitionStyle = .crossDissolve
        vc.closure = completion
        vc.categories = categories
        from.present(vc, animated: true, completion: nil)
    }
}

extension CategoryPickerVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CategoryCell.className) as! CategoryCell
        cell.set(categories[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        closure?(categories[indexPath.row])
        dismiss(animated: true, completion: nil)
    }
}
