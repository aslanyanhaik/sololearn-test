//
//  ArticlesVC.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit
import Kingfisher

class ArticlesVC: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    var categories = [ObjectCategory]()
    var selectedCategory = ObjectCategory()

    @IBAction func showCategoryVCPressed(_ sender: Any) {
        CategoryPickerVC.show(from: self, categories: categories) {[weak self] category in
            self?.selectedCategory = category
            self?.collectionView.reloadData()
            if category.articles.isEmpty {
                self?.spinner.startAnimating()
                self?.fetchArticles()
            }
        }
    }
    
    func fetchCategories() {
        ArticleManager().allCategories {[weak self] (response) in
            switch response {
            case .failure(let error):
                self?.showAlert(title: "Error", message: error?.localizedDescription ?? "Something went wrong")
            case .objects(let categories):
                self?.categories = categories
            }
        }
    }
    
    func fetchArticles() {
        ArticleManager().articles(for: selectedCategory) {[weak self] (response) in
            self?.spinner.stopAnimating()
            switch response {
            case .failure(let error):
                self?.showAlert(title: "Error", message: error?.localizedDescription ?? "Something went wrong")
            case .objects(let articles):
                guard let selectedCategory = self?.selectedCategory else { return }
                selectedCategory.articles.append(contentsOf: articles)
                self?.collectionView.reloadData() //should change to performBatchUpdates
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spinner.startAnimating()
        fetchCategories()
        fetchArticles()
    }
}

extension ArticlesVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDataSourcePrefetching, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedCategory.articles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ArticleCell.className, for: indexPath) as! ArticleCell
        cell.set(selectedCategory.articles[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.bounds.width - 45) / 2
        let height = width / 3 * 5
        return CGSize(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, prefetchItemsAt indexPaths: [IndexPath]) {
        indexPaths.forEach { (indexPath) in
            if let urlString = self.selectedCategory.articles[indexPath.row].thumbnailURLString, let url = URL(string: urlString) {
                ImageDownloader.default.downloadImage(with: url)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let articlesCount = selectedCategory.articles.count
        let index = articlesCount > 10 ? 10 : 1
        if indexPath.row == articlesCount - index {
            fetchArticles()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = storyboard?.instantiateViewController(withIdentifier: ArticleDetailsVC.className) as! ArticleDetailsVC
        vc.article = selectedCategory.articles[indexPath.row]
        show(vc, sender: self)
    }
}
