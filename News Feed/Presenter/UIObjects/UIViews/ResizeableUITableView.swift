//
//  ResizeableUITableView.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/27/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class ResizeableUITableView: UITableView {
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if !self.bounds.size.equalTo(self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            let intrinsicContentSize = self.contentSize
            return intrinsicContentSize
        }
    }
    
    func setup() {
        self.isScrollEnabled = false
        self.bounces = false
    }
}
