//
//  CategoryCell.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/21/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    func set(_ category: ObjectCategory) {
        titleLabel.text = category.title
    }
}
