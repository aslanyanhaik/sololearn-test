//
//  WordCell.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/27/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class WordCell: UITableViewCell {
    
    @IBOutlet weak var wordLabel: UILabel!
    
    func set(_ word: ObjectWord?) {
        guard let word = word else { return }
        wordLabel.text = "\(word.word) (\(word.count))"
    }    
}
