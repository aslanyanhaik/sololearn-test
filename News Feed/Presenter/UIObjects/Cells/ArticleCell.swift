//
//  ArticleCell.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/21/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    func set(_ article: ObjectArticle?) {
        titleLabel.text = article?.title
        if let date = article?.date {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateLabel.text = dateFormatter.string(from: date)
        }
        if let urlString = article?.thumbnailURLString {
            thumbnailImageView.kf.setImage(with: URL(string: urlString), placeholder: UIImage(named: "articlePlaceholder"))
        } else {
            thumbnailImageView.image = UIImage(named: "articlePlaceholder")
        }
    }
}
