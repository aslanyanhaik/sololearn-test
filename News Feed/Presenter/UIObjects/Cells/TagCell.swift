//
//  TagCell.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/24/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import UIKit

class TagCell: UICollectionViewCell {
    
    
    @IBOutlet weak var tagLabel: UILabel!
    
    func set(_ tag: String?) {
        tagLabel.text = tag
    }
    
}
