//
//  ArticleManager.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

class ArticleManager: Networkable {
    
    func allCategories(_ completion: @escaping CompletionBlock<ObjectCategory>) {
        objects(ObjectCategory.self, urlString: URLBuilder.url(location: .sections)) { (response) in
            completion(response)
        }
    }
    
    func articles(for category: ObjectCategory?, _ completion: @escaping CompletionBlock<ObjectArticle>) {
        guard let page = category?.page else {completion(.failure(nil)); return }
        var parameters = ["show-fields": "bodyText,thumbnail", "page-size": String(40), "page": String(page + 1)]
        if let id = category?.id {
            parameters["section"] = id
        }
        objects(ObjectArticle.self, urlString: URLBuilder.url(location: .content, parameters: parameters)) { (response) in
            switch response {
            case .objects(_): category?.page += 1
            default: break
            }
            completion(response)
        }
    }
}
