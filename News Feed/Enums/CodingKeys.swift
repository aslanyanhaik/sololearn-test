//
//  CodingKeys.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

public enum CodingKeys: String, CodingKey {
    case response = "response"
    case results = "results"
    case id = "id"
    case title = "webTitle"
    case date = "webPublicationDate"
    case fields = "fields"
    case thumbnail = "thumbnail"
    case bodyText = "bodyText"
    case category = "sectionName"
}
