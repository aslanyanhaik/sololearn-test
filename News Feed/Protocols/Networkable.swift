//
//  Networkable.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

protocol Networkable {
    func objects<T>(_ objectType: T.Type, urlString: String, completion: @escaping CompletionBlock<T>) where T: Decodable
}


