//
//  Typealias.swift
//  News Feed
//
//  Created by Haik Aslanyan on 9/20/18.
//  Copyright © 2018 Haik Aslanyan. All rights reserved.
//

import Foundation

public typealias CompletionBlock<T> = (_ response: NetworkResponse<T>) -> Void
